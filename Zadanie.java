package Zadanie;

import java.io.IOException;
import java.sql.*;
import java.util.InputMismatchException;
import java.util.Scanner;

public class Zadanie {

    static private String lastname;
    static private String name;
    static private int zP;
    static private int nal;
    static private int sum;

    private static void getConnectInBase() {
        try (Connection con = DriverManager.getConnection
                ("jdbc:mysql://localhost:3306/velobaza", "root", "root");
             Statement vBaze = con.createStatement()) {
            vBaze.execute("INSERT INTO zadanie.income(lastname, name, sum) VALUES" +
                    " ('" + lastname + "','" + name + "','" + sum + "')");
        } catch (SQLException e) {
            System.out.println(e);
        }

    }

    private static void getShowBase() {
        try (Connection con = DriverManager.getConnection
                ("jdbc:mysql://localhost:3306/velobaza", "root", "root");
             Statement loki = con.createStatement()) {
            ResultSet res = loki.executeQuery
                    ("SELECT name,lastname,sum FROM zadanie.income WHERE lastname='" + lastname + "'");
            while (res.next()) {
                String fam = res.getString("lastname");
                String nam = res.getString("name");
                int sum = res.getInt("sum");
                System.out.println(nam + " " + fam + " " + sum);
                System.out.println();
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
    }

    private static void getDelete() {
        try {
            System.out.println("Вы точно хотите удалить злодея ? yes/no");
            Scanner prov=new Scanner(System.in);
            String verify=prov.next("yes");
            try (Connection con = DriverManager.getConnection
                    ("jdbc:mysql://localhost:3306/velobaza", "root", "root");
                 Statement delit = con.createStatement()) {
                delit.executeUpdate("DELETE FROM zadanie.income WHERE lastname='" + lastname + "'");
                System.out.println("Злодей успешно удалён");
            } catch (SQLException e) {
                System.out.println(e);
            }
        } catch (InputMismatchException e) {
            System.out.println("Неправильно ввели форму, повторите попытку");
        }
    }

    public static void main(String args[]) throws IOException, SQLException {

        System.out.println("\tДобро пожаловать в мой первый проект !!!\n");
        char choise;
        int i;
        boolean g=false;
        System.out.println("change test");

        try {
            Class.forName("com.mysql.jdbc.Driver");
        } catch (ClassNotFoundException e){
            System.out.println(e);
        }

        for (i=0;i<1000;i++) {

            if (g) {
                System.out.println("Давай до.. свиданья!!!!");
                break;
            } else {
                do {
                    System.out.println("                Меню");
                    System.out.println("\n\tДля добавления в базу, нажмите 1" +
                            "\n\tДля просмотра данных из базы, нажмите 2" +
                            "\n\tДля удаления из базы, нажмите 3" +
                            "\n\tДля выхода из программы, нажмите 4");
                    choise = (char) System.in.read();
                } while (choise < '1' || choise > '4');

                switch (choise) {
                    case '1':
                        System.out.println("Укажите Фамилию, Имя, уровень ЗП в руб, налог на прибыль в % (целое число)");
                        try {
                            Scanner sc = new Scanner(System.in);
                            lastname = sc.next();
                            name = sc.next();
                            zP = Integer.parseInt(sc.next());
                            nal = Integer.parseInt(sc.next());
                            sum = zP - zP * nal / 100;
                        } catch (NumberFormatException e) {
                            System.out.println("Неправльно ввели данные, попробуйте снова");
                        }
                        getConnectInBase();
                        System.out.println("Ваши данные внесены в базу");
                        break;
                    case '2':
                        System.out.println("Укажите фамилию");
                        Scanner sh = new Scanner(System.in);
                        lastname = sh.next();
                        getShowBase();
                        break;
                    case '3':
                        System.out.println("Для удаления из базы, напишите фамилию злодея");
                        Scanner del = new Scanner(System.in);
                        lastname = del.next();
                        getDelete();
                        break;
                    case '4':
                        g = true;
                        break;
                }
            }
        }
    }
}

